package com.kozachuk.homework.luxoft.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

@JsonSerialize
public class Elvl {
    private String isin;
    private BigDecimal value;

    public Elvl(String isin, BigDecimal value) {
        this.isin = isin;
        this.value = value;
    }

    public String getIsin() {
        return isin;
    }

    public BigDecimal getValue() {
        return value;
    }
}
