package com.kozachuk.homework.luxoft.dto;

public enum ResponseDescription {
    ISIN("B-HMW-001", "ISIN value is wrong."),
    ASK("B-HMW-002", "ASK value is wrong."),
    BID("B-HMW-003", "BID value is wrong."),
    UNKNOWN("B-HMW-004", "Unknown error. Please contact with system administrator.");


    ResponseDescription(String errorCode, String message){
        this.errorCode = errorCode;
        this.message = message;
    }

    private String message;
    private String errorCode;

    public String getMessage() {
        return message;
    }

    public String getErrorCode() {
        return errorCode;
    }
}
