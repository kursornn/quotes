package com.kozachuk.homework.luxoft.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.math.BigDecimal;

@JsonSerialize
public class Quote {
    private String isin;
    private BigDecimal bid;
    private BigDecimal ask;

    public Quote(String isin, BigDecimal bid, BigDecimal ask) {
        this.isin = isin;
        this.bid = bid;
        this.ask = ask;
    }

    public String getIsin() {
        return isin;
    }

    public BigDecimal getBid() {
        return bid;
    }

    public BigDecimal getAsk() {
        return ask;
    }
}
