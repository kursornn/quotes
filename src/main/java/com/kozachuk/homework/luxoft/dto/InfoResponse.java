package com.kozachuk.homework.luxoft.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import java.util.Currency;

@JsonSerialize
public class InfoResponse {
    private Currency elvl;

    public InfoResponse(Currency elvl) {
        this.elvl = elvl;
    }

    public Currency getElvl() {
        return elvl;
    }
}
