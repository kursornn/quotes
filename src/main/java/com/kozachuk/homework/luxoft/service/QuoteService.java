package com.kozachuk.homework.luxoft.service;

import com.kozachuk.homework.luxoft.dto.Elvl;
import com.kozachuk.homework.luxoft.dto.Quote;
import com.kozachuk.homework.luxoft.exception.ValidationException;
import com.kozachuk.homework.luxoft.repository.QuoteRepository;
import com.kozachuk.homework.luxoft.validator.QuoteValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

@Component
public class QuoteService {
    public static final String NAME = "quoteService";
    private ConcurrentHashMap<String, BigDecimal> elvls = new ConcurrentHashMap<>();
    private QuoteRepository repository;
    private QuoteValidator quoteValidator;

    @Autowired
    public QuoteService(@Qualifier(QuoteRepository.NAME) QuoteRepository repository,
                        @Qualifier(QuoteValidator.NAME) QuoteValidator quoteValidator){
        this.repository = repository;
        this.quoteValidator = quoteValidator;
    }

    public void store(Quote quote) throws ValidationException {
        quoteValidator.validate(quote);
        repository.add(quote);

        elvls.computeIfAbsent(quote.getIsin(), value -> quote.getBid() != null ? quote.getBid() : quote.getAsk());

        elvls.computeIfPresent(quote.getIsin(), (key, value) -> {
            if(value == null) return quote.getAsk();

            BigDecimal result = null;
            BigDecimal currentBID = quote.getBid() == null ? value : quote.getBid();

            if(value.compareTo(currentBID) < 0) result = quote.getBid();
            if(value.compareTo(quote.getAsk()) >= 0) result = quote.getAsk();

            return result == null ? value : result;
        });
    }

    public List<String> getQuotes() {
        return Collections.list(elvls.keys());
    }

    public Quote getQuote(String isin) {
        return repository.get(isin);
    }

    public Elvl getElvl(String isin) {
        BigDecimal value = elvls.get(isin);
        return new Elvl(isin, value);
    }
}
