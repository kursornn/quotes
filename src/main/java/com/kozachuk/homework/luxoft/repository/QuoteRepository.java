package com.kozachuk.homework.luxoft.repository;

import com.kozachuk.homework.luxoft.dto.Quote;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository(QuoteRepository.NAME)
public class QuoteRepository {
    public static final String NAME = "quoteRepository";
    private JdbcTemplate jdbcTemplate;
    private final String INSERT_QUOTE = "insert into quotes(isin, bid, ask) values(?,?,?)";
    private final String FIND_QUOTE = "select bid, ask from quotes, (select isin, max(id) last from quotes where isin = ? group by isin) last where id = last.last";

    @Autowired
    public QuoteRepository(JdbcTemplate jdbcTemplate){
        this.jdbcTemplate = jdbcTemplate;
    }

    public void add(Quote quote){
        jdbcTemplate.update(INSERT_QUOTE, new Object[] {quote.getIsin(), quote.getBid(), quote.getAsk()});
    }

    public Quote get(String isin) {
        return jdbcTemplate.query(
                  FIND_QUOTE
                , new Object[]{ isin }
                , (rs, rownum) -> new Quote(isin, rs.getBigDecimal("bid"), rs.getBigDecimal("ask")))
            .stream().findFirst().get();
    }
}
