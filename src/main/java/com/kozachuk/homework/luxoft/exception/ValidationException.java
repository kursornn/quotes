package com.kozachuk.homework.luxoft.exception;

import com.kozachuk.homework.luxoft.dto.ResponseDescription;

public class ValidationException extends Exception {
    private ResponseDescription responseDescription;
    private String message;

    public ValidationException(ResponseDescription responseDescription, String message) {
        this.responseDescription = responseDescription;
        this.message = message;
    }

    public ResponseDescription getResponseDescription() {
        return responseDescription;
    }

    @Override
    public String getMessage() {
        return message;
    }
}
