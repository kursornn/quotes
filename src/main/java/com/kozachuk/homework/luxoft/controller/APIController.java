package com.kozachuk.homework.luxoft.controller;

import com.kozachuk.homework.luxoft.dto.Elvl;
import com.kozachuk.homework.luxoft.dto.ErrorResponse;
import com.kozachuk.homework.luxoft.dto.Quote;
import com.kozachuk.homework.luxoft.dto.ResponseDescription;
import com.kozachuk.homework.luxoft.exception.ValidationException;
import com.kozachuk.homework.luxoft.service.QuoteService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1.0/")
public class APIController {
    private QuoteService quoteService;

    public APIController(@Qualifier(QuoteService.NAME) QuoteService quoteService){
        this.quoteService = quoteService;
    }

    @RequestMapping(value = "/quotes", method = RequestMethod.GET, produces = "application/json")
    public List<String> getQuotes(){
        return quoteService.getQuotes();
    }

    @RequestMapping(value = "/quotes", method = RequestMethod.POST, produces = "application/json")
    @ResponseStatus(HttpStatus.CREATED)
    public void addQuote(@RequestBody Quote quote) throws ValidationException {
        quoteService.store(quote);
    }

    @GetMapping(value = "/quotes/{isin}")
    public Quote getQuoteByIsin(@PathVariable("isin") String isin) {
        Quote result = quoteService.getQuote(isin);
        return result;
    }

    @GetMapping(value = "/quotes/{isin}/elvl")
    public Elvl getElvlByIsin(@PathVariable("isin") String isin){
        Elvl result = quoteService.getElvl(isin);
        return result;
    }

    @ExceptionHandler(ValidationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse exceptionHandler(ValidationException exception) {
        return new ErrorResponse( exception.getResponseDescription().getErrorCode()
                                , exception.getResponseDescription().getMessage()
                                , exception.getMessage());

    }

    @ExceptionHandler(Throwable.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ErrorResponse exceptionHandler() {
        return new ErrorResponse(ResponseDescription.UNKNOWN.getErrorCode(), ResponseDescription.UNKNOWN.getMessage(), null);
    }
}
