package com.kozachuk.homework.luxoft.validator;

import com.kozachuk.homework.luxoft.dto.Quote;
import com.kozachuk.homework.luxoft.dto.ResponseDescription;
import com.kozachuk.homework.luxoft.exception.ValidationException;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component(QuoteValidator.NAME)
public class QuoteValidator {
    public static final String NAME = "quoteValidator";

    public void validate(Quote quote) throws ValidationException {
        isinValidation(quote);
        if(quote.getAsk() == null) throw new ValidationException(ResponseDescription.ASK, "Value cannot be empty.");

        commonValueValidation(quote.getAsk(), ResponseDescription.ASK);
        commonValueValidation(quote.getBid(), ResponseDescription.BID);
        compareAskAndBID(quote);
    }

    protected void isinValidation(Quote quote) throws ValidationException {
        String isin = quote.getIsin();
        if(isin == null || isin.isEmpty()) throw new ValidationException(ResponseDescription.ISIN, "Isin cannot be empty.");

        if(isin.contains(" ")) throw new ValidationException(ResponseDescription.ISIN, "Isin shouldn't contain spaces.");

        int length = isin.length();
        if(length != 12) throw new ValidationException(ResponseDescription.ISIN, "Isin Length should be 12 symbols.");
    }

    protected void commonValueValidation(BigDecimal value, ResponseDescription description) throws ValidationException {
        if(value != null && value.compareTo(BigDecimal.ZERO) == -1) throw new ValidationException(description, "Value cannot be less than zero.");
    }

    protected void compareAskAndBID(Quote quote) throws ValidationException {
        BigDecimal bid = quote.getBid();
        BigDecimal ask = quote.getAsk();

        if(bid != null && ask != null && bid.compareTo(ask) > 0){
            throw new ValidationException(ResponseDescription.BID, "BID should not be greater than ASK.");
        }
    }
}
