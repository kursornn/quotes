package com.kozachuk.homework.luxoft.performance;

import com.kozachuk.homework.luxoft.Application;
import org.jsmart.zerocode.core.runner.parallel.ZeroCodeLoadRunner;
import org.junit.runners.model.InitializationError;

public class ZerocodeSpringBootRunner extends ZeroCodeLoadRunner {

    public ZerocodeSpringBootRunner(Class<?> klass) throws InitializationError {
        super(klass);
        Application.start();
    }

}
