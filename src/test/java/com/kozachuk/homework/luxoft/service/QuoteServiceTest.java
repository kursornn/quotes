package com.kozachuk.homework.luxoft.service;

import com.kozachuk.homework.luxoft.dto.Elvl;
import com.kozachuk.homework.luxoft.dto.Quote;
import com.kozachuk.homework.luxoft.exception.ValidationException;
import com.kozachuk.homework.luxoft.repository.QuoteRepository;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
class QuoteServiceTest {
    @Autowired
    private QuoteService service;
    @Autowired
    private QuoteRepository repository;

    private final String isinValue = "RU000A0JX0J1";
    private final String isinValue2 = "RU000A0JX0J2";
    private final BigDecimal bid = new BigDecimal(1.0);
    private final BigDecimal ask = new BigDecimal(2.0);

    private final BigDecimal bid2 = new BigDecimal(0.5);
    private final BigDecimal ask2 = new BigDecimal(0.9);

    @Test
    void store_add_correct_quote() throws ValidationException {
        Quote quote = new Quote(isinValue, bid, ask);

        service.store(quote);
        Quote result = repository.get(isinValue);

        Assert.assertEquals(ask, result.getAsk());
        Assert.assertEquals(bid, result.getBid());
        Assert.assertEquals(isinValue, result.getIsin());
    }

    @Test
    void store_quote_bid_should_be_elvl() throws ValidationException {
        Quote quote = new Quote(isinValue, bid, ask);

        service.store(quote);
        Elvl elvl = service.getElvl(isinValue);

        Assert.assertEquals(elvl.getIsin(), isinValue);
        Assert.assertEquals(elvl.getValue(), bid);
    }

    @Test
    void store_quote_bid_is_null_elvl_is_ask() throws ValidationException {
        Quote quote = new Quote(isinValue2, null, ask);

        service.store(quote);
        Elvl elvl = service.getElvl(isinValue2);

        Assert.assertEquals(isinValue2, elvl.getIsin());
        Assert.assertEquals(ask, elvl.getValue());
    }

    @Test
    void store_2_quote_for_1_document() throws ValidationException {
        BigDecimal newBid = bid.add(new BigDecimal(0.5));
        Quote quote1 = new Quote(isinValue, bid, ask);
        Quote quote2 = new Quote(isinValue, newBid, ask);

        service.store(quote1);
        service.store(quote2);

        Elvl elvl = service.getElvl(isinValue);

        Assert.assertEquals(isinValue, elvl.getIsin());
        Assert.assertEquals(newBid, elvl.getValue());
    }

    @Test
    void store_2_quote_for_1_document_ask_less_that_elvl() throws ValidationException {
        Quote quote1 = new Quote(isinValue, bid, ask);
        Quote quote2 = new Quote(isinValue, bid2, ask2);

        service.store(quote1);
        service.store(quote2);

        Elvl elvl = service.getElvl(isinValue);

        Assert.assertEquals(isinValue, elvl.getIsin());
        Assert.assertEquals(ask2, elvl.getValue());
    }


}
