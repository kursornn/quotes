package com.kozachuk.homework.luxoft.validator;

import com.kozachuk.homework.luxoft.dto.Quote;
import com.kozachuk.homework.luxoft.exception.ValidationException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest
public class QuoteValidatorTest {
    @Autowired
    QuoteValidator validator;

    @Test(expected = ValidationException.class)
    public void isinValidationTest_valueEmpty() throws ValidationException {
        String isinValue = "";
        Quote quote = new Quote(isinValue, new BigDecimal(1.0), new BigDecimal(2.0));
        validator.isinValidation(quote);
    }

    @Test(expected = ValidationException.class)
    public void isinValidationTest_valueNull() throws ValidationException {
        String isinValue = null;
        Quote quote = new Quote(isinValue, new BigDecimal(1.0), new BigDecimal(2.0));
        validator.isinValidation(quote);
    }

    @Test(expected = ValidationException.class)
    public void isinValidationTest_space_as_value() throws ValidationException {
        String isinValue = " ";
        Quote quote = new Quote(isinValue, new BigDecimal(1.0), new BigDecimal(2.0));
        validator.isinValidation(quote);
    }

    @Test(expected = ValidationException.class)
    public void isinValidationTest_wrong_length_more_than_12() throws ValidationException {
        String isinValue = "RU000A0JX0J2_";
        Quote quote = new Quote(isinValue, new BigDecimal(1.0), new BigDecimal(2.0));
        validator.isinValidation(quote);
    }

    @Test(expected = ValidationException.class)
    public void isinValidationTest_wrong_length_less_than_12() throws ValidationException {
        String isinValue = "RU000A0JX0J";
        Quote quote = new Quote(isinValue, new BigDecimal(1.0), new BigDecimal(2.0));
        validator.isinValidation(quote);
    }


    @Test(expected = ValidationException.class)
    public void compare_dib_more_than_ask() throws ValidationException {
        String isinValue = "RU000A0JX0J2";
        Quote quote = new Quote(isinValue, new BigDecimal(2.0), new BigDecimal(1.0));
        validator.validate(quote);
    }

    @Test
    public void compare_dib_less_than_ask() throws ValidationException {
        String isinValue = "RU000A0JX0J2";
        Quote quote = new Quote(isinValue, new BigDecimal(1.0), new BigDecimal(2.0));
        validator.validate(quote);
    }

    @Test
    public void bid_is_null() throws ValidationException {
        String isinValue = "RU000A0JX0J2";
        Quote quote = new Quote(isinValue, null, new BigDecimal(2.0));
        validator.validate(quote);
    }

    @Test(expected = ValidationException.class)
    public void ask_is_null() throws ValidationException {
        String isinValue = "RU000A0JX0J2";
        Quote quote = new Quote(isinValue, null, null);
        validator.validate(quote);
    }

}
